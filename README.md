# emacs config

This is my personal emacs config. I have declared
[.emacs bankruptcy](https://www.emacswiki.org/emacs/DotEmacsBankruptcy)
multiple times, so take this with a grain of salt. The basis of this
config are two packages: [org-mode](https://orgmode.org/) and
[use-package](https://github.com/jwiegley/use-package).

Take whatever you want. Honor free software.

## Ideas

### Tabs

It would be cool if at some point I could
use emacs with tabs, similar to swaywm and tmux.
The builtin tab-mode is probably all I need for that.
https://www.gnu.org/software/emacs/manual/html_node/emacs/Tab-Bars.html
The modifier-prefix would course have to be C-x.
It would probably break a few keybindings, but not many that I use, apparently.

### Only show the menu bar when it is needed

There is already some attempt in there, but it doesn't work reliably yet.
